#!/bin/bash

mkdir -p deps
cd deps

git clone https://gitlab.freedesktop.org/xrdesktop/g3k.git
cd g3k
git checkout $1
meson build --prefix /usr
ninja -C build install
cd ../..
