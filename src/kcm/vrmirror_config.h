/*
 * KWin XRDesktop Plugin
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef KWIN_VRMIRROR_CONFIG_H
#define KWIN_VRMIRROR_CONFIG_H

#include <kcmodule.h>

class KShortcutsEditor;

namespace KWin
{

class VRMirrorEffectConfig : public KCModule
{
    Q_OBJECT
public:
    explicit VRMirrorEffectConfig(QWidget *parent = nullptr, const QVariantList &args = QVariantList());
    ~VRMirrorEffectConfig();

public Q_SLOTS:
    virtual void save();
    virtual void load();
    virtual void defaults();

private:
    KShortcutsEditor *mShortcutEditor;
};

} // namespace

#endif
