#ifndef OLD_EPOXY_COMPAT_H
#define OLD_EPOXY_COMPAT_H

#ifndef PFNGLCREATEMEMORYOBJECTSEXTPROC
typedef void (APIENTRYP PFNGLCREATEMEMORYOBJECTSEXTPROC) (GLsizei n, GLuint *memoryObjects);
#endif

#ifndef PFNGLMEMORYOBJECTPARAMETERIVEXTPROC
typedef void (APIENTRYP PFNGLMEMORYOBJECTPARAMETERIVEXTPROC) (GLuint memoryObject, GLenum pname, const GLint *params);
#endif

#ifndef PFNGLGETMEMORYOBJECTPARAMETERIVEXTPROC
typedef void (APIENTRYP PFNGLGETMEMORYOBJECTPARAMETERIVEXTPROC) (GLuint memoryObject, GLenum pname, GLint *params);
#endif

#ifndef PFNGLIMPORTMEMORYFDEXTPROC
typedef void (APIENTRYP PFNGLIMPORTMEMORYFDEXTPROC) (GLuint memory, GLuint64 size, GLenum handleType, GLint fd);
#endif

#ifndef PFNGLTEXSTORAGEMEM2DEXTPROC
typedef void (APIENTRYP PFNGLTEXSTORAGEMEM2DEXTPROC) (GLenum target, GLsizei levels, GLenum internalFormat, GLsizei width, GLsizei height, GLuint memory, GLuint64 offset);
#endif

#ifndef PFNGLDELETEMEMORYOBJECTSEXTPROC
typedef void (APIENTRYP PFNGLDELETEMEMORYOBJECTSEXTPROC) (GLsizei n, const GLuint *memoryObjects);
#endif

#ifndef GL_HANDLE_TYPE_OPAQUE_FD_EXT
#define GL_HANDLE_TYPE_OPAQUE_FD_EXT      0x9586
#endif

#ifndef GL_DEDICATED_MEMORY_OBJECT_EXT
#define GL_DEDICATED_MEMORY_OBJECT_EXT    0x9581
#endif

#ifndef glCreateMemoryObjectsEXT
EPOXY_PUBLIC void (EPOXY_CALLSPEC *epoxy_glCreateMemoryObjectsEXT)(GLsizei n, GLuint * memoryObjects);
#define glCreateMemoryObjectsEXT epoxy_glCreateMemoryObjectsEXT
#endif

#ifndef glMemoryObjectParameterivEXT
EPOXY_PUBLIC void (EPOXY_CALLSPEC *epoxy_glMemoryObjectParameterivEXT)(GLuint memoryObject, GLenum pname, const GLint * params);
#define glMemoryObjectParameterivEXT epoxy_glMemoryObjectParameterivEXT
#endif

#ifndef glGetMemoryObjectParameterivEXT
EPOXY_PUBLIC void (EPOXY_CALLSPEC *epoxy_glGetMemoryObjectParameterivEXT)(GLuint memoryObject, GLenum pname, GLint * params);
#define glGetMemoryObjectParameterivEXT epoxy_glGetMemoryObjectParameterivEXT
#endif

#ifndef glImportMemoryFdEXT
EPOXY_PUBLIC void (EPOXY_CALLSPEC *epoxy_glImportMemoryFdEXT)(GLuint memory, GLuint64 size, GLenum handleType, GLint fd);
#define glImportMemoryFdEXT epoxy_glImportMemoryFdEXT
#endif

#ifndef glTexStorageMem2DEXT
EPOXY_PUBLIC void (EPOXY_CALLSPEC *epoxy_glTexStorageMem2DEXT)(GLenum target, GLsizei levels, GLenum internalFormat, GLsizei width, GLsizei height, GLuint memory, GLuint64 offset);
#define glTexStorageMem2DEXT epoxy_glTexStorageMem2DEXT
#endif

#ifndef glDeleteMemoryObjectsEXT
EPOXY_PUBLIC void (EPOXY_CALLSPEC *epoxy_glDeleteMemoryObjectsEXT)(GLsizei n, const GLuint * memoryObjects);
#define glDeleteMemoryObjectsEXT epoxy_glDeleteMemoryObjectsEXT
#endif

#endif
