/*
 * KWin XRDesktop Plugin
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#undef signals

#include "VRMirror.h"

#include <KGlobalAccel>
#include <KLocalizedString>
#include <kwinglplatform.h>
#include <QAction>
#include <QtDebug>

#include <gdk/gdk.h>

#include <gulkan.h>
#include <g3k.h>
#include <xrd.h>

// segfault handler
#include <csignal>

// ugh
#undef Unsorted
#undef None
#undef False
#undef True
#include <QtDBus>

// typedefs in case epoxy is too old
#include "old_epoxy_compat.h"

#include "kwingltexture2.h"

void GLAPIENTRY MessageCallback(GLenum source,
                                GLenum type,
                                GLuint id,
                                GLenum severity,
                                GLsizei length,
                                const GLchar *message,
                                const void *userParam)
{
    (void) source;
    (void) id;
    (void) length;
    (void) userParam;
    fprintf(stderr,
            "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
            type,
            severity,
            message);
}

static VRMirror *instance = nullptr;

static bool isExcludedFromMirroring(KWin::EffectWindow *kwin_window)
{
    /* yes, we do indeed not mirror NULL */
    if (kwin_window == NULL)
        return TRUE;

    /* Skip SteamVR monitor windows */
    if (!kwin_window->windowClass().compare("vrmonitor vrmonitor")) {
        return TRUE;
    }

    return kwin_window->isDesktop() || kwin_window->isDock() || kwin_window->isDNDIcon()
           || kwin_window->width() < 20 || kwin_window->height() < 20;
}

static bool _isDeleted(KWin::EffectWindow *w)
{
#if KWIN_EFFECT_API_VERSION_MINOR >= 227
    bool isDeleted = w->isDeleted();
#else
    const QVariant isDeletedVariant = w->parent()->property("deleted");
    bool isDeleted = isDeletedVariant.toBool();
#endif
    return isDeleted;
}

static XrdWindow *_kwinWindowToXrdWindow(VRMirror *self,
                                         KWin::EffectWindow *w,
                                         bool includeDeleted = false)
{
    if (isExcludedFromMirroring(w))
        return NULL;

    /* this window has been closed, but still receives callbacks for a while */
    if (!includeDeleted && _isDeleted(w))
        return NULL;

    if (self->onlyCurrentWorkspace && !w->isOnCurrentDesktop())
        return NULL;

    XrdWindow *xrdWin = xrd_shell_lookup_window(self->xrdShell, w);
    return xrdWin;
}

/** Menus, ContextMenus, etc. that should be fixed to a parent window and may
 * not be individually moved */
static bool isChildWindow(KWin::EffectWindow *win)
{
    return win->isPopupMenu() || win->isDropdownMenu() || win->isTooltip() || win->isComboBox()
           || win->isDialog() || win->isMenu();
}

// functions take char* so we can't use literals
static char keepAboveString[] = "keepAbove";
static char keepBelowString[] = "keepBelow";

static void setWindowProperty(KWin::EffectWindow *win, char *propertyname, bool value)
{
    const QVariant variant = win->parent()->property(propertyname);
    if (variant.isValid()) {
        win->parent()->setProperty(propertyname, value);
    }
}

/*
static void putAbove(KWin::EffectWindow *win)
{
    setWindowProperty(win, keepBelowString, false);
    setWindowProperty(win, keepAboveString, true);
    if (KWin::effects->activeWindow() != win) {
        KWin::effects->activateWindow(win);
    }
}
*/

static void putBelow(KWin::EffectWindow *win)
{
    // TODO: setting the above/below state from an effect was always a hack, but crashes now.
#if KWIN_EFFECT_API_VERSION_MINOR < 236
    // TODO: putting all windows on putBelow is annoying on --replace or crash.
    // Therefore just steal any keepAbove status for now
    setWindowProperty(win, keepAboveString, false);
    // setWindowProperty(win, keepBelowString, true);
#endif
}

/* TODO: before this function can be used it must support monitors of different sizes at the same
 * time, and has to know to avoud panel areas.
 */
/*
static void fitWindowOnDesktop(KWin::EffectWindow *win)
{
    win->unminimize();
    QRect windowrect = win->geometry();
    QRect screenarea = KWin::effects->virtualScreenGeometry();

    // y grows DOWN
    if (windowrect.top() < screenarea.top()) {
        int ydiff = screenarea.top() - windowrect.top();
        windowrect.translate(0, ydiff);
    }
    if (windowrect.bottom() > screenarea.bottom()) {
        int ydiff = windowrect.bottom() - screenarea.bottom();
        windowrect.translate(0, -ydiff);
    }

    if (windowrect.left() < screenarea.left()) {
        int xdiff = screenarea.left() - windowrect.left();
        windowrect.translate(xdiff, 0);
    }
    if (windowrect.right() > screenarea.right()) {
        int xdiff = windowrect.right() - screenarea.right();
        windowrect.translate(-xdiff, 0);
    }

    qDebug() << "Screen:" << screenarea.topLeft() << "," << screenarea.bottomRight()
             << "  Window:" << win->geometry().topLeft() << "," << win->geometry().bottomRight();
    if (windowrect == win->geometry()) {
        qDebug() << "Nothing to move!";
    } else {
        qDebug() << "Moving window" << win->caption() << "from" << win->geometry().topLeft() << ","
                 << win->geometry().bottomRight() << "to" << windowrect.topLeft() << ","
                 << windowrect.bottomRight();
        KWin::effects->moveWindow(win, windowrect.topLeft(), true, 5.0);
    }
}
*/

/*
 * kwin 5.24 changes the KWIN_EFFECT_FACTORY_SUPPORTED_ENABLED macro.
 * Reference: https://github.com/KDE/kwin/commit/66352bfc87b410b1e91b819fbe9eac4515ee9823
 *
 * The last time KWIN_EFFECT_API_VERSION_MINOR was bumped was in 5.22.
 * Reference: https://github.com/KDE/kwin/commit/38996d97256c8f5c274da97d866fb55103129c46
 *
 * There doesn't seem to be a way to ifdef based on kwin version as kwinconfig.h only
 * defines a string like KWIN_PLUGIN_VERSION_STRING "5.24.0".
 *
 * Fortunately the commit changing the define also introduced a new define.
 * As a workaround, ifdef on that new define.
 */
#ifdef KWIN_PLUGIN_FACTORY_NAME
KWIN_EFFECT_FACTORY_SUPPORTED_ENABLED(VRMirror,
                                      "vrmirror.json",
                                      return VRMirror::supported();
                                      , return VRMirror::enabledByDefault();)
#else
KWIN_EFFECT_FACTORY_SUPPORTED_ENABLED(VRMirrorFactory,
                                      VRMirror,
                                      "vrmirror.json",
                                      return VRMirror::supported();
                                      , return VRMirror::enabledByDefault();)
#endif

/* Coordinate space: 0 == x: left, y == 0: top */
static graphene_point_t _windowToDesktopCoordinates(WindowWrapper *vrWin,
                                                    graphene_point_t *positionOnWindow)
{
    KWin::EffectWindow *kwinWindow = vrWin->kwinWindow;
#if KWIN_EFFECT_API_VERSION_MINOR >= 235
    QPointF pos = kwinWindow->pos();
#else
    QPoint pos = kwinWindow->pos();
#endif
    graphene_point_t positionOnDesktop = {static_cast<float>(pos.x() + positionOnWindow->x),
                                          static_cast<float>(pos.y() + positionOnWindow->y)};
    return positionOnDesktop;
}

static void _click_cb(XrdShell *client, XrdClickEvent *event, VRMirror *self);
static void _move_cursor_cb(XrdShell *client, XrdMoveCursorEvent *event, VRMirror *self);
static void _keyboard_press_cb(XrdShell *client, G3kKeyEvent *event, VRMirror *self);

static void _stateChangeCallback(GxrContext *gxr, GxrStateChangeEvent *event, VRMirror *self)
{
    g_print("state change: %d!\n", event->state_change);
    (void) gxr;
    switch (event->state_change) {
    case GXR_STATE_SHUTDOWN:
        self->framecycle = FALSE;
        self->rendering = FALSE;
        QTimer::singleShot(0, self, SLOT(deactivateVRMirror()));
        break;
    case GXR_STATE_FRAMECYCLE_START:
        self->framecycle = TRUE;
        break;
    case GXR_STATE_FRAMECYCLE_STOP:
        self->framecycle = FALSE;
        break;
    case GXR_STATE_RENDERING_START:
        self->rendering = TRUE;
        g_print("kwin: Start rendering windows\n");
        break;
    case GXR_STATE_RENDERING_STOP:
        self->rendering = FALSE;
        g_print("kwin: Stop rendering windows\n");
        break;
    }
}

static void _connectClientSources(VRMirror *self)
{
    self->clickSource = g_signal_connect(self->xrdShell, "click-event", (GCallback) _click_cb, self);
    self->moveSource = g_signal_connect(self->xrdShell,
                                        "move-cursor-event",
                                        (GCallback) _move_cursor_cb,
                                        self);
    self->keyboardSource = g_signal_connect(self->xrdShell,
                                            "keyboard-press-event",
                                            (GCallback) _keyboard_press_cb,
                                            self);
    self->stateChangeSource = g_signal_connect(g3k_context_get_gxr(self->g3k),
                                               "state-change-event",
                                               (GCallback) _stateChangeCallback,
                                               self);
}

static void _disconnectClientSources(VRMirror *self)
{
    g_signal_handler_disconnect(self->xrdShell, self->clickSource);
    g_signal_handler_disconnect(self->xrdShell, self->moveSource);
    g_signal_handler_disconnect(self->xrdShell, self->keyboardSource);
    g_signal_handler_disconnect(self->xrdShell, self->stateChangeSource);
    self->clickSource = 0;
    self->moveSource = 0;
    self->keyboardSource = 0;
    self->stateChangeSource = 0;
}

VRMirror::VRMirror()
    : m_vrmirrorRunning(false)
{
    instance = this;

    qDebug() << "Starting xrdesktop plugin init.";

    auto *toggleAction = new QAction(this);
    toggleAction->setObjectName(QStringLiteral("Toggle"));
    toggleAction->setText(i18n("Toggle Mirroring Windows to VR with xrdesktop"));
    KGlobalAccel::self()->setDefaultShortcut(toggleAction, {});
    KGlobalAccel::self()->setShortcut(toggleAction, {});

    //! @todo see if this needs a fix
    // daa351a39829013404d005369cb76fabfc1384ea
#if KWIN_EFFECT_API_VERSION_MINOR < 236
    KWin::effects->registerGlobalShortcut({}, toggleAction);
#endif

    connect(toggleAction, &QAction::triggered, this, &VRMirror::toggleScreenVRMirror);
    connect(KWin::effects, &KWin::EffectsHandler::windowClosed, this, &VRMirror::slotWindowClosed);
    connect(KWin::effects, &KWin::EffectsHandler::windowAdded, this, &VRMirror::slotWindowAdded);
    connect(KWin::effects,
            &KWin::EffectsHandler::cursorShapeChanged,
            this,
            &VRMirror::slotUpdateCursorTexture);

    if (!QDBusConnection::sessionBus().registerObject(QStringLiteral("/XR"),
                                                      this,
                                                      QDBusConnection::ExportScriptableProperties
                                                          | QDBusConnection::ExportScriptableSignals
                                                          | QDBusConnection::ExportScriptableSlots)) {
        qDebug() << "Failed to register DBus object";
    }

    connect(KWin::effects, &KWin::EffectsHandler::windowDamaged, this, &VRMirror::damaged);

    QString tracePaintTimeStr = QProcessEnvironment::systemEnvironment().value("TRACE_PAINT_TIME",
                                                                               "");
    tracePaintTime = tracePaintTimeStr != "";

    QString uploadOnlyDamagedStr = QProcessEnvironment::systemEnvironment()
                                       .value("UPLOAD_ONLY_DAMAGED", "TRUE");
    uploadOnlyDamaged = uploadOnlyDamagedStr != "";

    QString onlyCurrentWorkspaceStr = QProcessEnvironment::systemEnvironment()
                                          .value("MIRROR_ONLY_CURRENT_WORKSPACE", "");
    onlyCurrentWorkspace = onlyCurrentWorkspaceStr != "";

    QString glDebugEnabled = QProcessEnvironment::systemEnvironment().value("GL_DEBUG_ENABLED", "");
    if (glDebugEnabled != "") {
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(MessageCallback, 0);
    }

    qDebug() << "Initializing xrdesktop plugin  successful.";
}

void VRMirror::damaged(KWin::EffectWindow *w)
{
    if (!m_vrmirrorRunning)
        return;

    XrdWindow *xrdWin = _kwinWindowToXrdWindow(this, w);
    if (!xrdWin)
        return;

    WindowWrapper *native = nullptr;
    g_object_get(xrdWin, "native", &native, NULL);
    native->framesToRender = std::max(RENDER_NUM_FRAMES_AFTER_DAMAGE, native->framesToRender);
}

static bool isDamaged(VRMirror *self, KWin::EffectWindow *kwin_window)
{
    XrdWindow *xrdWin = _kwinWindowToXrdWindow(self, kwin_window);
    if (!xrdWin)
        return false;

    WindowWrapper *native = nullptr;
    g_object_get(xrdWin, "native", &native, NULL);
    return native->framesToRender > 0;
}
static void damageProcessed(VRMirror *self, KWin::EffectWindow *kwin_window)
{
    XrdWindow *xrdWin = _kwinWindowToXrdWindow(self, kwin_window);
    if (!xrdWin)
        return;

    WindowWrapper *native = nullptr;
    g_object_get(xrdWin, "native", &native, NULL);
    native->framesToRender -= 1;
}

void VRMirror::restoreAboveBelowStatus()
{
    G3kObjectManager *manager = g3k_context_get_manager (g3k);
    GSList *windows = g3k_object_manager_get_objects (manager);
    for (GSList *l = windows; l; l = l->next) {
        XrdWindow *xrdWin = XRD_WINDOW(l->data);

        WindowWrapper *vrWin;
        g_object_get(xrdWin, "native", &vrWin, NULL);

// TODO: setting the above/below state from an effect was always a hack, but crashes now.
#if KWIN_EFFECT_API_VERSION_MINOR < 236
        setWindowProperty(vrWin->kwinWindow, keepAboveString, vrWin->keepAboveOrig);
        setWindowProperty(vrWin->kwinWindow, keepBelowString, vrWin->keepBelowOrig);
#endif
    }
}

static void _ensure_on_workspace(KWin::EffectWindow *kwin_window)
{
    uint32_t currentDesktop = KWin::effects->currentDesktop();

    // property lookup still works, but the new function is faster!
#if KWIN_EFFECT_API_VERSION_MINOR >= 227
    QVector<uint> windowDesktops = kwin_window->desktops();
#else
    const QVariant variant = kwin_window->parent()->property("x11DesktopIds");
    QVector<uint> windowDesktops = variant.value<QVector<uint>>();
#endif

    // If the list is empty it means the window is on all desktops
    if (windowDesktops.size() == 0)
        return;

    // On X11 this list will always have a length of 1, on Wayland can be any
    // subset.
    // TODO: wayland
    if (currentDesktop != windowDesktops[0]) {
        KWin::effects->setCurrentDesktop(windowDesktops[0]);
    }
}

static void _click_cb(XrdShell *client, XrdClickEvent *event, VRMirror *self)
{
    (void) client;
    (void) self;
    // g_print ("click: %f, %f\n", event->position->x, event->position->y);

    XrdWindow *xrdWin = XRD_WINDOW (event->object);
    WindowWrapper *vrWin;
    g_object_get(xrdWin, "native", &vrWin, NULL);

    if (!vrWin || !vrWin->kwinWindow || _isDeleted(vrWin->kwinWindow))
        return;

    _ensure_on_workspace(vrWin->kwinWindow);

    if (KWin::effects->activeWindow() != vrWin->kwinWindow) {
        KWin::effects->activateWindow(vrWin->kwinWindow);
    }

    graphene_point_t xy = _windowToDesktopCoordinates(vrWin, event->position);

    qDebug() << (event->state ? "Pressing " : "Releasing ") << " button " << event->button << "at"
             << xy.x << ", " << xy.y;

    input_synth_click(self->synth, xy.x, xy.y, event->button, event->state);
}

static guint64 last;
static void _move_cursor_cb(XrdShell *client, XrdMoveCursorEvent *event, VRMirror *self)
{
    (void) client;
    (void) self;
    // g_print ("click: %f, %f\n", event->position->x, event->position->y);

    if (event->ignore) {
        qDebug() << "Ignored event";
        return;
    }

    XrdWindow *xrdWin = event->window;
    WindowWrapper *vrWin;
    g_object_get(xrdWin, "native", &vrWin, NULL);

    if (!vrWin || !vrWin->kwinWindow || _isDeleted(vrWin->kwinWindow))
        return;

    _ensure_on_workspace(vrWin->kwinWindow);

    if (KWin::effects->activeWindow() != vrWin->kwinWindow) {
        KWin::effects->activateWindow(vrWin->kwinWindow);
    }

    if (vrWin->kwinWindow->isUserMove() || vrWin->kwinWindow->isUserResize()) {
        qDebug() << "Not moving mouse while user is moving or resizing window!";
        return;
    }

    graphene_point_t xy = _windowToDesktopCoordinates(vrWin, event->position);

    guint64 now = g_get_monotonic_time();
    // qDebug() << "Synth input after: " << (now - last) / 1000.;
    last = now;
    input_synth_move_cursor(self->synth, xy.x, xy.y);
}

static void _keyboard_press_cb(XrdShell *client, G3kKeyEvent *event, VRMirror *self)
{
    if (!xrd_shell_get_keyboard_window(client)) {
        qDebug() << "ERROR: No keyboard window!";
        return;
    }

    XrdWindow *keyboardXrdWin = xrd_shell_get_keyboard_window(client);
    WindowWrapper *keyboardVrWin;
    g_object_get(keyboardXrdWin, "native", &keyboardVrWin, NULL);

    if (!keyboardVrWin || !keyboardVrWin->kwinWindow || _isDeleted(keyboardVrWin->kwinWindow))
        return;

    _ensure_on_workspace(keyboardVrWin->kwinWindow);

    if (KWin::effects->activeWindow() != keyboardVrWin->kwinWindow) {
        KWin::effects->activateWindow(keyboardVrWin->kwinWindow);
    }

    qDebug() << "Keyboard Input:" << event->string;
    input_synth_characters(self->synth, event->string);
}

void _updateCursorImage(VRMirror *mirror, KWin::PlatformCursorImage *cursorImage)
{
    QPoint cursorHotspot = cursorImage->hotSpot();

    uint32_t newWidth = cursorImage->image().width();
    uint32_t newHeight = cursorImage->image().height();

    /* rare case that crashes if we don't return early */
    if (newWidth == 0 || newHeight == 0)
        return;

    /* The CGImageRef color space is set to the sRGB color space.
     * https://doc.qt.io/qt-5/qimage.html */
    QImage formatted = cursorImage->image().convertToFormat(QImage::Format_RGBA8888);
    uchar *rgba = formatted.bits();

    /* srgb data not converted to linear, even with GDK_COLORSPACE_RGB */
    GdkPixbuf *pixbuf = gdk_pixbuf_new_from_data(rgba,
                                                 GDK_COLORSPACE_RGB,
                                                 TRUE,
                                                 8,
                                                 newWidth,
                                                 newHeight,
                                                 4 * newWidth,
                                                 NULL,
                                                 NULL);
    GulkanContext *gulkan = g3k_context_get_gulkan(mirror->g3k);

    G3kCursor *cursor = xrd_shell_get_desktop_cursor(mirror->xrdShell);
    GulkanTexture *cached_texture = g3k_plane_get_texture (G3K_PLANE (cursor));

    G3kContext *g3k = xrd_shell_get_g3k(mirror->xrdShell);
    VkImageLayout layout = g3k_context_get_upload_layout(g3k);

    if (cached_texture == NULL || gulkan_texture_get_extent(cached_texture).width != newWidth
        || gulkan_texture_get_extent(cached_texture).height != newHeight) {
        cached_texture = gulkan_texture_new_from_pixbuf(gulkan,
                                                        pixbuf,
                                                        VK_FORMAT_R8G8B8A8_SRGB,
                                                        layout,
                                                        false);
        g3k_plane_set_texture(G3K_PLANE (cursor), cached_texture);
        g3k_cursor_set_hotspot(cursor, cursorHotspot.x(), cursorHotspot.y());
    } else {
        gulkan_texture_upload_pixbuf(cached_texture, pixbuf, layout);
        // TODO: need to submit?
        g3k_cursor_set_hotspot(cursor, cursorHotspot.x(), cursorHotspot.y());
    }

    // qDebug() << "Uploading" << width << "x" << height;
}

void VRMirror::slotUpdateCursorTexture()
{
    if (!m_vrmirrorRunning)
        return;

    KWin::PlatformCursorImage cursorImage = KWin::effects->cursorImage();
    _updateCursorImage(this, &cursorImage);
}

VRMirror::~VRMirror()
{
    qDebug() << "VRMirror plugin destroyed";

    // when disabling opengl compositing we get instantly destroyed
    if (m_vrmirrorRunning) {
        deactivateVRMirror();
    }
    QDBusConnection::sessionBus().unregisterObject(QStringLiteral("/XR"));
}

bool VRMirror::supported()
{
    return KWin::effects->isOpenGLCompositing();
}

bool VRMirror::enabledByDefault()
{
    return supported();
}

void VRMirror::glibIterate()
{
    // xrdesktop creates a glib main context for us, so we use that default one
    // with the NULL parameter
    while (g_main_context_pending(NULL)) {
        g_main_context_iteration(NULL, FALSE);
    }
}

#if KWIN_EFFECT_API_VERSION_MINOR >= 232
void VRMirror::prePaintScreen(KWin::ScreenPrePaintData &data, std::chrono::milliseconds presentTime)
{
    KWin::effects->prePaintScreen(data, presentTime);
    lastPrePaint = QTime::currentTime();
}
#else
void VRMirror::prePaintScreen(KWin::ScreenPrePaintData &data, int time)
{
    KWin::effects->prePaintScreen(data, time);
    lastPrePaint = QTime::currentTime();
}
#endif

void VRMirror::drawWindow(KWin::EffectWindow *w,
                          int mask,
                          const QRegion &region,
                          KWin::WindowPaintData &data)
{
    if (m_vrmirrorRunning && _kwinWindowToXrdWindow(this, w)) {
        // qDebug() << "draw window " << w->caption();
        w->addRepaintFull();
    }

    KWin::effects->drawWindow(w, mask, region, data);
}

void VRMirror::setPositionFromDesktop(XrdWindow *xrdWin)
{
    WindowWrapper *vrWin;
    g_object_get(xrdWin, "native", &vrWin, NULL);
    int desktopHeight = KWin::effects->virtualScreenGeometry().height();
    int desktopWidth = KWin::effects->virtualScreenGeometry().width();

#if KWIN_EFFECT_API_VERSION_MINOR >= 235
    QPointF p = vrWin->kwinWindow->frameGeometry().center();
#elif KWIN_EFFECT_API_VERSION_MINOR >= 233
    QPoint p = vrWin->kwinWindow->frameGeometry().center();
#else
    QPoint p = vrWin->kwinWindow->geometry().center();
#endif
    p.setY(desktopHeight * 0.75 - p.y());
    p.setX(p.x() - desktopWidth / 2.);

    graphene_point3d_t point = {static_cast<float>(p.x() / pixelsPerMeter),
                                static_cast<float>(p.y() / pixelsPerMeter),
                                -8.0f + ((float) num_windows) / 3.f};

    graphene_matrix_t transform;
    graphene_matrix_init_translate(&transform, &point);

    g3k_object_set_matrix(G3K_OBJECT (xrdWin), &transform);

    G3kObjectManager *manager = g3k_context_get_manager (g3k);
    g3k_object_manager_save_reset_transformation (manager, G3K_OBJECT (xrdWin));
}

extern "C" {
/* forward declared to avoid include mess */
extern void (*glXGetProcAddress(const GLubyte *procname))(void);
// pulled in through epoxy egl header: extern void (*eglGetProcAddress(const
// char *procname))(void);
}
typedef void *(*getProcAddressFunc)(const GLubyte *procname);

static bool _load_gl_symbol(const char *name, void **func)
{
    getProcAddressFunc getProcAddress = nullptr;
    if (KWin::GLPlatform::instance()->platformInterface() == KWin::GlxPlatformInterface) {
        getProcAddress = (getProcAddressFunc) &glXGetProcAddress;
    } else if (KWin::GLPlatform::instance()->platformInterface() == KWin::EglPlatformInterface) {
        getProcAddress = (getProcAddressFunc) &eglGetProcAddress;
    } else {
        qDebug() << "ERROR: Can only load function pointers on GLX or EGL!";
        return false;
    }

    *func = getProcAddress((GLubyte *) name);

    if (!*func) {
        qDebug() << "Error: Failed to resolve required GL symbol" << name;
        return false;
    }
    return TRUE;
}

static bool _loadGLExtPtrs()
{
    if (!_load_gl_symbol("glCreateMemoryObjectsEXT", (void **) &glCreateMemoryObjectsEXT))
        return false;
    if (!_load_gl_symbol("glMemoryObjectParameterivEXT", (void **) &glMemoryObjectParameterivEXT))
        return false;
    if (!_load_gl_symbol("glGetMemoryObjectParameterivEXT",
                         (void **) &glGetMemoryObjectParameterivEXT))
        return false;
    if (!_load_gl_symbol("glImportMemoryFdEXT", (void **) &glImportMemoryFdEXT))
        return false;
    if (!_load_gl_symbol("glTexStorageMem2DEXT", (void **) &glTexStorageMem2DEXT))
        return false;
    if (!_load_gl_symbol("glDeleteMemoryObjectsEXT", (void **) &glDeleteMemoryObjectsEXT))
        return false;

    return true;
}

static bool _glExtPtrsLoaded()
{
    return glCreateMemoryObjectsEXT != nullptr && glMemoryObjectParameterivEXT != nullptr
           && glGetMemoryObjectParameterivEXT != nullptr && glImportMemoryFdEXT != nullptr
           && glTexStorageMem2DEXT != nullptr && glDeleteMemoryObjectsEXT != nullptr;
}

/* returns a gulkanTexture AND updates the respective KWin::GLTexture inside vrWin */
static GulkanTexture *_allocateTexture(XrdShell *xrdShell,
                                       WindowWrapper *vrWin,
                                       int width,
                                       int height)
{
    qDebug() << "Reallocationg GL texture for" << vrWin->kwinWindow->caption() << "---"
             << (vrWin->offscreenGLTexture ? vrWin->offscreenGLTexture->width() : 0) << "x"
             << (vrWin->offscreenGLTexture ? vrWin->offscreenGLTexture->height() : 0) << "->"
             << width << "x" << height << "GL Texture ID:"
             << (vrWin->offscreenGLTexture ? vrWin->offscreenGLTexture->texture() : 0);

    if (!_glExtPtrsLoaded()) {
        _loadGLExtPtrs();

        if (!_glExtPtrsLoaded()) {
            qDebug() << "Failed to load GL functions!";
            return NULL;
        }
    }

    G3kContext *g3k = xrd_shell_get_g3k(xrdShell);
    VkImageLayout layout = g3k_context_get_upload_layout(g3k);
    GulkanContext *gulkan = g3k_context_get_gulkan(g3k);

    gsize size;
    int fd;
    VkExtent2D extent = { .width = static_cast<uint32_t>(width), .height = static_cast<uint32_t>(height) };
    GulkanTexture *gkTexture
        = gulkan_texture_new_export_fd(gulkan, extent, VK_FORMAT_R8G8B8A8_SRGB, layout, &size, &fd);

    GLuint glTexId;
    glGenTextures(1, &glTexId);
    glBindTexture(GL_TEXTURE_2D, glTexId);
    glTexParameteri (GL_TEXTURE_2D,GL_TEXTURE_TILING_EXT, GL_OPTIMAL_TILING_EXT);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLuint glExtMemObject = 0;
    glCreateMemoryObjectsEXT(1, &glExtMemObject);
    GLint glDedicatedMem = GL_TRUE;
    glMemoryObjectParameterivEXT(glExtMemObject, GL_DEDICATED_MEMORY_OBJECT_EXT, &glDedicatedMem);
    glGetMemoryObjectParameterivEXT(glExtMemObject, GL_DEDICATED_MEMORY_OBJECT_EXT, &glDedicatedMem);
    glImportMemoryFdEXT(glExtMemObject, size, GL_HANDLE_TYPE_OPAQUE_FD_EXT, fd);
    glTexStorageMem2DEXT(GL_TEXTURE_2D, 1, GL_SRGB8_ALPHA8, width, height, glExtMemObject, 0);
    glDeleteMemoryObjectsEXT(1, &glExtMemObject);

    qDebug() << "Imported vk memory size" << size << " from fd" << fd << "into OpenGL memory object"
             << glExtMemObject;

    if (vrWin->offscreenGLTexture) {
        delete vrWin->offscreenGLTexture;
    }
    vrWin->offscreenGLTexture = new KWin::GLTexture2(GL_SRGB8_ALPHA8, width, height, 1, glTexId);

    return gkTexture;
}

void VRMirror::upload_window(XrdWindow *xrdWin)
{
    if (xrdWin == NULL) {
        qDebug() << "Window null";
        return;
    }

    if (!g3k_object_is_visible(G3K_OBJECT(xrdWin))) {
        return;
    }

    WindowWrapper *vrWin;
    g_object_get(xrdWin, "native", &vrWin, NULL);

    // already closed
    if (!vrWin->kwinWindow) {
        return;
    }

    if (uploadOnlyDamaged && !isDamaged(this, vrWin->kwinWindow)) {
        // qDebug() << "Skipping upload of undamaged" <<
        // vrWin->kwinWindow->caption();
        return;
    }
    // qDebug() << "Damaged (" << windowDamaged[vrWin->kwinWindow] << " ):" <<
    // vrWin->kwinWindow->caption();

    damageProcessed(this, vrWin->kwinWindow);

    // this texture copy method is taken from the screenshot effect
    // https://github.com/KDE/kwin/blob/2043161889f57f4a34eaf7852f5f9f758fb00790/effects/screenshot/screenshot.cpp#L99

    // https://github.com/KDE/kwin/commit/63a866d98c6c004f5625b13089f45e52d457bb2e
    // https://github.com/KDE/kwin/commit/992753c24a3efbeb306d1130f5278ca3394caede
#if KWIN_EFFECT_API_VERSION_MINOR >= 234
    KWin::WindowPaintData d;

    const int width = vrWin->kwinWindow->width();
    const int height = vrWin->kwinWindow->height();

    d.setXTranslation(-vrWin->kwinWindow->x());
    d.setYTranslation(-vrWin->kwinWindow->y());

    int mask = VRMirror::PAINT_WINDOW_TRANSFORMED | VRMirror::PAINT_WINDOW_TRANSLUCENT;
    QMatrix4x4 projection;
    projection.ortho(QRect(0, 0, width, height));
    d.setProjectionMatrix(projection);

    GulkanTexture *cached_texture = g3k_plane_get_texture(G3K_PLANE(xrdWin));
    if (!vrWin->offscreenGLTexture || width != vrWin->offscreenGLTexture->width()
        || height != vrWin->offscreenGLTexture->height()) {
        cached_texture = _allocateTexture(xrdShell, vrWin, width, height);
        xrd_window_set_flip_y(xrdWin, true);

        QScopedPointer<KWin::GLFramebuffer> target;
        target.reset(new KWin::GLFramebuffer(vrWin->offscreenGLTexture));
        if (!target->valid()) {
            return;
        }
        KWin::GLFramebuffer::pushFramebuffer(target.data());

        KWin::effects->drawWindow(vrWin->kwinWindow, mask, KWin::infiniteRegion(), d);
        g3k_plane_set_texture(G3K_PLANE(xrdWin), cached_texture);

        KWin::GLFramebuffer::popFramebuffer();
    } else {
        QScopedPointer<KWin::GLFramebuffer> target;
        target.reset(new KWin::GLFramebuffer(vrWin->offscreenGLTexture));
        if (!target->valid()) {
            return;
        }
        KWin::GLFramebuffer::pushFramebuffer(target.data());

        KWin::effects->drawWindow(vrWin->kwinWindow, mask, KWin::infiniteRegion(), d);

        KWin::effects->drawWindow(vrWin->kwinWindow, mask, KWin::infiniteRegion(), d);
        // TODO submit?

        KWin::GLFramebuffer::popFramebuffer();
    }
#else
    KWin::WindowPaintData d(vrWin->kwinWindow);

    const int width = vrWin->kwinWindow->width();
    const int height = vrWin->kwinWindow->height();

    d.setXTranslation(-vrWin->kwinWindow->x());
    d.setYTranslation(-vrWin->kwinWindow->y());

    int mask = VRMirror::PAINT_WINDOW_TRANSFORMED | VRMirror::PAINT_WINDOW_TRANSLUCENT;
    QMatrix4x4 projection;
    projection.ortho(QRect(0, 0, width, height));
    d.setProjectionMatrix(projection);

    GulkanTexture *cached_texture = g3k_plane_get_texture(G3K_PLANE(xrdWin));
    if (!vrWin->offscreenGLTexture || width != vrWin->offscreenGLTexture->width()
        || height != vrWin->offscreenGLTexture->height()) {
        cached_texture = _allocateTexture(xrdShell, vrWin, width, height);
        xrd_window_set_flip_y(xrdWin, true);

        QScopedPointer<KWin::GLRenderTarget> target;
        target.reset(new KWin::GLRenderTarget(*vrWin->offscreenGLTexture));
        if (!target->valid()) {
            return;
        }
        KWin::GLRenderTarget::pushRenderTarget(target.data());

        KWin::effects->drawWindow(vrWin->kwinWindow, mask, KWin::infiniteRegion(), d);
        g3k_plane_set_texture(G3K_PLANE(xrdWin), cached_texture);

        KWin::GLRenderTarget::popRenderTarget();
    } else {
        QScopedPointer<KWin::GLRenderTarget> target;
        target.reset(new KWin::GLRenderTarget(*vrWin->offscreenGLTexture));
        if (!target->valid()) {
            return;
        }
        KWin::GLRenderTarget::pushRenderTarget(target.data());

        KWin::effects->drawWindow(vrWin->kwinWindow, mask, KWin::infiniteRegion(), d);

        KWin::effects->drawWindow(vrWin->kwinWindow, mask, KWin::infiniteRegion(), d);
        // TODO submit?

        KWin::GLRenderTarget::popRenderTarget();
    }
#endif
}

void VRMirror::postPaintWindow(KWin::EffectWindow *w)
{
    if (m_vrmirrorRunning) {
        XrdWindow *xrdWin = _kwinWindowToXrdWindow(this, w);
        if (!xrdWin) {
            KWin::effects->postPaintWindow(w);
            return;
        }

        if (!rendering) {
          KWin::effects->postPaintWindow(w);
          return;
        }

        upload_window(xrdWin);
    }

    KWin::effects->postPaintWindow(w);
}

void VRMirror::postPaintScreen()
{
    KWin::effects->postPaintScreen();

    QTime now = QTime::currentTime();
    if (tracePaintTime) {
        qDebug() << "Paint screen took" << lastPrePaint.msecsTo(now) << "ms; "
                 << "Frametime" << lastPostPaint.msecsTo(now) << "ms";
    }

    lastPostPaint = now;
}

void VRMirror::slotWindowClosed(KWin::EffectWindow *w)
{
    if (!m_vrmirrorRunning) {
        return;
    }

    qDebug() << "Window closed: " << w->caption();
    XrdWindow *xrdWin = _kwinWindowToXrdWindow(this, w, true);
    if (!xrdWin) {
        qDebug() << "Closed window without xrdwin!";
        return;
    }

    WindowWrapper *vrWin;
    g_object_get(xrdWin, "native", &vrWin, NULL);
    if (!vrWin) {
        qDebug() << "Closed window without native!";
        return;
    }

    delete vrWin;
    g_object_set(xrdWin, "native", NULL, NULL);

    xrd_shell_remove_window(instance->xrdShell, xrdWin);

    xrd_window_close(xrdWin);
    g_object_unref(xrdWin);

    num_windows--;
}

void VRMirror::slotWindowAdded(KWin::EffectWindow *w)
{
    if (!m_vrmirrorRunning) {
        return;
    }

    mapWindow(w, false);
}

XrdWindow *VRMirror::mapWindow(KWin::EffectWindow *win, bool force)
{
    if (win->width() > 5 && win->height() > 5) {
    } else {
        qDebug() << "Window too small, not adding:" << win->caption() << win->width() << "x"
                 << win->height();
        return NULL;
    }

    if (force) {
    } else {
        if (isExcludedFromMirroring(win)) {
            qDebug() << win->caption() << "is one of the excluded classes, skipping...";
            return NULL;
        }

        if (onlyCurrentWorkspace && !win->isOnCurrentDesktop()) {
            qDebug() << "Not mirroring window on other workspace:" << win->caption();
            return NULL;
        }
    }

    // TODO: make sure window is contained in current desktop by improving this function
    // fitWindowOnDesktop(win);

    WindowWrapper *vrWin = new WindowWrapper(win);

    bool isChild = isChildWindow(win);

    /* The window that a modal/dialog/menu is created from should have been
     * activated. */
    KWin::EffectWindow *parentWindow = KWin::effects->activeWindow();
    bool isParentMirrored = isChild && !isExcludedFromMirroring(parentWindow)
                            && _kwinWindowToXrdWindow(this, parentWindow) != NULL;
    bool hasParent = parentWindow && isParentMirrored;

    std::string title_str = win->caption().toStdString();
    const char *window_title = title_str.c_str();

    if (isChild && !hasParent) {
        qDebug() << "Warning:" << window_title
                 << "should be child but has no parent, trying hovered window!";

        XrdWindow *hoveredXrdWin = xrd_shell_get_synth_hovered(xrdShell);
        if (hoveredXrdWin && XRD_IS_WINDOW(hoveredXrdWin)) {
            WindowWrapper *hoveredWindow = nullptr;
            g_object_get(hoveredXrdWin, "native", &hoveredWindow, NULL);

            if (hoveredWindow && !_isDeleted(hoveredWindow->kwinWindow)) {
                qDebug() << "hovered window will be used as parent!";
                parentWindow = hoveredWindow->kwinWindow;
                hasParent = true;
            }
        }
    }

    G3kContext *g3k = xrd_shell_get_g3k(xrdShell);
    const float ppm = 300;

    VkExtent2D size_pixels = {static_cast<uint32_t>(win->width()),
                              static_cast<uint32_t>(win->height())};
    graphene_size_t size_meters = g3k_extent_to_size (&size_pixels, ppm);

    XrdWindow *xrdWin = xrd_window_new (g3k, window_title, vrWin, size_pixels,
                                         &size_meters);

    G3kObject *parentObject = NULL;
    if (isChild && hasParent) {
        parentObject = G3K_OBJECT(_kwinWindowToXrdWindow(this, parentWindow));
    }

    xrd_shell_add_window(xrdShell, xrdWin, parentObject, !(isChild && hasParent), win);

    if (isChild && hasParent) {
        XrdWindow *parentXrdWin = _kwinWindowToXrdWindow(this, parentWindow);
        WindowWrapper *parentVrWin = NULL;
        if (XRD_IS_WINDOW(parentXrdWin)) {
            g_object_get(parentXrdWin, "native", &parentVrWin, NULL);
        }

#if KWIN_EFFECT_API_VERSION_MINOR >= 235
        QRectF parentGeometry = parentVrWin->kwinWindow->frameGeometry();
        QPointF parentCenter = parentGeometry.center();
        QPointF childCenter = win->frameGeometry().center();

        QPointF offsetPixels = childCenter - parentCenter;

#elif KWIN_EFFECT_API_VERSION_MINOR >= 233
        QRect parentGeometry = parentVrWin->kwinWindow->frameGeometry();
        QPoint parentCenter = parentGeometry.center();
        QPoint childCenter = win->frameGeometry().center();

        QPoint offsetPixels = childCenter - parentCenter;
#else
        QRect parentGeometry = parentVrWin->kwinWindow->geometry();
        QPoint parentCenter = parentGeometry.center();
        QPoint childCenter = win->geometry().center();

        QPoint offsetPixels = childCenter - parentCenter;
#endif

        graphene_point3d_t offset_point = {.x = g3k_to_meters(offsetPixels.x(), ppm),
                                           .y = -g3k_to_meters(offsetPixels.y(), ppm),
                                           .z = 0.1};
        G3kPose offset_pose = g3k_pose_new (&offset_point, NULL);

        g3k_object_set_local_pose (G3K_OBJECT (xrdWin), &offset_pose);

        xrd_window_add_child(parentXrdWin, xrdWin);

        qDebug() << "Adding child at" << childCenter << "to parent at" << parentCenter << ", diff "
                 << offsetPixels << "px = " << offset_point.x << ", " << offset_point.y << "m";
    } else {
        setPositionFromDesktop(xrdWin);
    }

    upload_window(xrdWin);

    // save status so it can be restored when deactivating VR mode
#if KWIN_EFFECT_API_VERSION_MINOR >= 227
    vrWin->keepAboveOrig = win->keepAbove();
    vrWin->keepBelowOrig = win->keepBelow();
#else
    const QVariant keepAboveVariant = win->parent()->property("keepAbove");
    vrWin->keepAboveOrig = keepAboveVariant.toBool();

    const QVariant keepBelowVariant = win->parent()->property("keepBelow");
    vrWin->keepBelowOrig = keepBelowVariant.toBool();
#endif

    putBelow(vrWin->kwinWindow);

    qDebug() << "Created window" << win->caption() << "KeepAbove:" << vrWin->keepAboveOrig
             << "KeepBelow:" << vrWin->keepBelowOrig << win->windowClass() << win->windowRole();

    num_windows++;

    return xrdWin;
}

void segfaultSigaction(int signal, siginfo_t *si, void *arg)
{
    (void) arg;
    printf("Caught segfault at address %p\n", si->si_addr);

    // reset signal handling strategy
    std::signal(signal, SIG_DFL);

    // try to not leave the user with a messed up window configuration before
    // exiting. hope this memory is still intact so we don't segfault there...
    if (instance && instance->m_vrmirrorRunning) {
        instance->restoreAboveBelowStatus();
    }

    // if this didn't crash, re-segfault to create a coredump
    raise(SIGSEGV);
}

void VRMirror::deactivateVRMirror()
{
    qDebug() << "deactivating VR mirror...";

    _disconnectClientSources(this);

    pollTimer->stop();
    delete pollTimer;
    pollTimer = 0;

    m_vrmirrorRunning = false;
    num_windows = 0;

    restoreAboveBelowStatus();

    g_object_unref(synth);
    synth = NULL;

    G3kObjectManager *manager = g3k_context_get_manager (g3k);
    GSList *windows = g3k_object_manager_get_objects (manager);
    for (GSList *l = windows; l; l = l->next) {
        XrdWindow *xrdWin = XRD_WINDOW(l->data);
        xrd_window_close(xrdWin);
        /* shell unref will do it anyway
        xrd_shell_remove_window(xrdShell, xrdWin); */
    }

    g_object_unref(xrdShell);
    xrdShell = NULL;

    std::signal(SIGSEGV, SIG_DFL);
}

void VRMirror::activateVRMirror()
{
    if (!KWin::effects->isOpenGLCompositing()) {
        qDebug() << "VR mirror only supported with OpenGL compositing!";
        return;
    }

    if (!g3k_settings_is_schema_installed("org.xrdesktop")) {
        qDebug() << "xrdesktop GSettings Schema not installed. Check your xrdesktop installation!";
        return;
    }

    xrdShell = xrd_shell_new();
    if (!xrdShell) {
        qDebug() << "Failed to initialize xrdesktop!";
        qDebug() << "Usually this is caused by a problem with the VR runtime.";
        return;
    }

    g3k = xrd_shell_get_g3k (xrdShell);

    struct sigaction sa;
    memset(&sa, 0, sizeof(struct sigaction));
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = segfaultSigaction;
    sa.sa_flags = SA_SIGINFO;
    sigaction(SIGSEGV, &sa, NULL);

    QString synth_method = QProcessEnvironment::systemEnvironment().value("INPUTSYNTH", "XDO");
    if (synth_method == "XI2") {
        qDebug() << "Using Synth Method: XI2";
        synth = INPUT_SYNTH(input_synth_new(INPUTSYNTH_BACKEND_XI2));
    } else {
        qDebug() << "Using Synth Method: XDO";
        synth = INPUT_SYNTH(input_synth_new(INPUTSYNTH_BACKEND_XDO));
    }

    if (!synth) {
        qDebug() << "Failed to initialize input synth";
        return;
    }

    slotUpdateCursorTexture();

    _connectClientSources(this);

    KWin::EffectWindowList order = KWin::effects->stackingOrder();
    for (KWin::EffectWindow *win : order) {
        qDebug() << "Mapping window " << win->caption();
        mapWindow(win, false);
    }

    qDebug() << "activating VR mirror...";
    pollTimer = new QTimer(this);
    pollTimer->setTimerType(Qt::PreciseTimer);
    pollTimer->start(1);
    connect(pollTimer, SIGNAL(timeout()), this, SLOT(glibIterate()));
    m_vrmirrorRunning = true;
    qDebug() << "VR Mirror activated";

    lastPostPaint = QTime::currentTime();

    g3k_context_start_renderer(g3k);
}

void VRMirror::toggleScreenVRMirror()
{
    if (m_vrmirrorRunning) {
        deactivateVRMirror();
    } else {
        activateVRMirror();
    }
}

bool VRMirror::isActive() const
{
    return m_vrmirrorRunning;
}

bool VRMirror::isHmdConnected() const
{
    /* TODO: Neither OpenVR nor OpenXR provide a "good" way of monitoring
     * VR hardware presence which is not slow/hogging resources.
     */
    return true;
}

void VRMirror::setActive(bool active)
{
    qDebug() << "dbus property change:" << active;
    // test if we even need to do something
    if (m_vrmirrorRunning == active) {
        return;
    }

    toggleScreenVRMirror();

    // only emit change signal if toggle did actually work
    if (m_vrmirrorRunning == active) {
        return;
    }
    emit activeChanged(m_vrmirrorRunning);
}

/* hack: Avoid warning (some environments may treat this warning as fatal):
 *
 * "SRC:/src/VRMirror.cpp"
 * includes the moc file "VRMirror.moc", but does not contain a Q_OBJECT, Q_GADGET,
 * Q_NAMESPACE, Q_NAMESPACE_EXPORT, K_PLUGIN_FACTORY, K_PLUGIN_FACTORY_WITH_JSON or
 * K_PLUGIN_CLASS_WITH_JSON macro.
 *
 * It is a bogus warning and KWIN_EFFECT_FACTORY_SUPPORTED_ENABLED will not embed
 * json metadata if .moc is not included.
 */
class _ : QObject
{
    Q_OBJECT
};

#include "VRMirror.moc"
