.templates_sha: &templates_sha e9a461877f8c7f9fed9fff5491067ec3c3472559

include:
  - project: 'freedesktop/ci-templates'
    ref: *templates_sha
    file: '/templates/arch.yml'
  - project: "freedesktop/ci-templates"
    ref: *templates_sha
    file: "/templates/ubuntu.yml"
  - project: "freedesktop/ci-templates"
    ref: *templates_sha
    file: "/templates/debian.yml"

variables:
  FDO_UPSTREAM_REPO: xrdesktop/kwin-effect-xrdesktop
  GULKAN_COMMIT: "c43a2e7d93a64e8107390d94b1622030c9426ffe"
  GXR_COMMIT: "2d5406114f89b4c6346eb9f24a111fedb30062ef"
  G3K_COMMIT: "8be29ea4524f662460dbaab2de486ad3fc296af3"
  XRDESKTOP_COMMIT: "6a54ae9de97ad20b0b74e39c2408c683b6d397c5"
  LIBINPUTSYNTH_COMMIT: "f369bcf1301aa8b6cac82d1656112e7a3bee2fd4"
  OPENXR_SDK_TAG: "release-1.0.26"
  SHADERC_TAG: "v2022.3"
  # increment to easily rebuild all images
  FDO_DISTRIBUTION_DATE: "2022-03-17"

stages:
  - container_prep
  - build_and_test
  - package
  - reprepro
  - trigger_packaging


# "Base" job for installing Gulkan, OpenXR SDK
.xrdesktop.base-job.build_deps:
  stage: container_prep
  variables:
    FDO_DISTRIBUTION_EXEC: |
      bash .gitlab-ci/dep_install/install_gulkan.sh $GULKAN_COMMIT && \
      bash .gitlab-ci/dep_install/install_openxr.sh $OPENXR_SDK_TAG && \
      bash .gitlab-ci/dep_install/install_gxr.sh $GXR_COMMIT && \
      bash .gitlab-ci/dep_install/install_shaderc.sh $SHADERC_TAG && \
      bash .gitlab-ci/dep_install/install_g3k.sh $G3K_COMMIT && \
      bash .gitlab-ci/dep_install/install_xrdesktop.sh $XRDESKTOP_COMMIT && \
      bash .gitlab-ci/dep_install/install_libinputsynth.sh $LIBINPUTSYNTH_COMMIT

# "Base" job for a Meson build
.xrdesktop.base-job.build:
  stage: build_and_test
  script:
    - cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr -GNinja -Bbuild
    - ninja -C build install


# variables shared by container_prep and build jobs
.xrdesktop.variables.arch:rolling:
  variables:
    FDO_DISTRIBUTION_TAG: "${FDO_DISTRIBUTION_DATE}.0"

.xrdesktop.variables.ubuntu:focal:
  variables:
    FDO_DISTRIBUTION_VERSION: "20.04"
    FDO_DISTRIBUTION_TAG: "${FDO_DISTRIBUTION_DATE}.0"

.xrdesktop.variables.ubuntu:jammy:
  variables:
    FDO_DISTRIBUTION_VERSION: "22.04"
    FDO_DISTRIBUTION_TAG: "${FDO_DISTRIBUTION_DATE}.0"

.xrdesktop.variables.debian:bookworm:
  variables:
    FDO_DISTRIBUTION_VERSION: "bookworm"
    FDO_DISTRIBUTION_TAG: "${FDO_DISTRIBUTION_DATE}.0"

.xrdesktop.variables.debian:bullseye:
  variables:
    FDO_DISTRIBUTION_VERSION: "bullseye"
    FDO_DISTRIBUTION_TAG: "${FDO_DISTRIBUTION_DATE}.0"

.xrdesktop.variables.debian:sid:
  variables:
    FDO_DISTRIBUTION_VERSION: "sid"
    FDO_DISTRIBUTION_TAG: "${FDO_DISTRIBUTION_DATE}.0"

.xrdesktop.variables.debian-based-packages:
  variables:
    CORE_REQUIRED_PACKAGES: "git gcc clang meson pkg-config libglib2.0-dev libgdk-pixbuf2.0-dev libvulkan-dev libgraphene-1.0-dev libcairo2-dev glslang-tools cmake libxxf86vm-dev libgl1-mesa-dev libvulkan-dev libx11-xcb-dev libxcb-dri2-0-dev libxcb-glx0-dev libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-randr0-dev libxrandr-dev libxxf86vm-dev mesa-common-dev libgtk-3-dev libjson-glib-dev ca-certificates python3-dev python-gi-dev libkf5configwidgets-dev libkf5globalaccel-dev libkf5i18n-dev libkf5service-dev libkf5xmlgui-dev kwin-dev qtdeclarative5-dev plasma-workspace-dev gettext libxdo-dev apt-utils reprepro build-essential devscripts debhelper dput-ng gettext-base gtk-doc-tools libglew-dev libglfw3-dev dh-python libcanberra-dev"

# === Archlinux ===

arch:container_prep:
  extends:
    - .xrdesktop.variables.arch:rolling
    - .fdo.container-build@arch # from ci-templates
    - .xrdesktop.base-job.build_deps
  stage: container_prep
  variables:
    FDO_DISTRIBUTION_PACKAGES: "pkgconf meson gdk-pixbuf2 vulkan-headers vulkan-icd-loader graphene cairo glslang glfw-x11 glew shaderc json-glib gcc clang git cmake gtk3 libffi pygobject-devel kwin extra-cmake-modules xdotool libcanberra"

arch:meson:gcc:
  extends:
    - .xrdesktop.variables.arch:rolling
    - .fdo.distribution-image@arch # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++

arch:meson:clang:
  extends:
    - .xrdesktop.variables.arch:rolling
    - .fdo.distribution-image@arch # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: clang
    CXX: clang++


# === Ubuntu Focal ===

ubuntu:focal:container_prep:
  stage: container_prep
  extends:
    - .xrdesktop.variables.ubuntu:focal
    - .fdo.container-build@ubuntu # from ci-templates
    - .xrdesktop.base-job.build_deps
    - .xrdesktop.variables.debian-based-packages
  variables:
    FDO_DISTRIBUTION_PACKAGES: "${CORE_REQUIRED_PACKAGES}"

ubuntu:focal:gcc:
  extends:
    - .xrdesktop.variables.ubuntu:focal
    - .fdo.distribution-image@ubuntu # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++
  before_script:
   - export PYTHONPATH=$PYTHONPATH:/usr/lib/python3.8/site-packages/ # Add standard python package path for tests


# === Ubuntu jammy ===

ubuntu:jammy:container_prep:
  stage: container_prep
  extends:
    - .xrdesktop.variables.ubuntu:jammy
    - .fdo.container-build@ubuntu # from ci-templates
    - .xrdesktop.base-job.build_deps
    - .xrdesktop.variables.debian-based-packages
  variables:
    FDO_DISTRIBUTION_PACKAGES: "${CORE_REQUIRED_PACKAGES}"

ubuntu:jammy:gcc:
  extends:
    - .xrdesktop.variables.ubuntu:jammy
    - .fdo.distribution-image@ubuntu # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++
  before_script:
   - export PYTHONPATH=$PYTHONPATH:/usr/lib/python3.8/site-packages/ # Add standard python package path for tests


# === Debian bookworm ===

debian:bookworm:container_prep:
  stage: container_prep
  extends:
    - .xrdesktop.variables.debian:bookworm
    - .fdo.container-build@debian # from ci-templates
    - .xrdesktop.base-job.build_deps
    - .xrdesktop.variables.debian-based-packages
  variables:
    FDO_DISTRIBUTION_PACKAGES: "${CORE_REQUIRED_PACKAGES}"

debian:bookworm:gcc:
  extends:
    - .xrdesktop.variables.debian:bookworm
    - .fdo.distribution-image@debian # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++


# === Debian bullseye ===

debian:bullseye:container_prep:
  stage: container_prep
  extends:
    - .xrdesktop.variables.debian:bullseye
    - .fdo.container-build@debian # from ci-templates
    - .xrdesktop.base-job.build_deps
    - .xrdesktop.variables.debian-based-packages
  variables:
    FDO_DISTRIBUTION_PACKAGES: "${CORE_REQUIRED_PACKAGES}"

debian:bullseye:gcc:
  extends:
    - .xrdesktop.variables.debian:bullseye
    - .fdo.distribution-image@debian # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++


# === Debian sid ===

debian:sid:container_prep:
  stage: container_prep
  extends:
    - .xrdesktop.variables.debian:sid
    - .fdo.container-build@debian # from ci-templates
    - .xrdesktop.base-job.build_deps
    - .xrdesktop.variables.debian-based-packages
  variables:
    FDO_DISTRIBUTION_PACKAGES: "${CORE_REQUIRED_PACKAGES}"

debian:sid:gcc:
  extends:
    - .xrdesktop.variables.debian:sid
    - .fdo.distribution-image@debian # from ci-templates
    - .xrdesktop.base-job.build
  variables:
    MESON_ARGS: -Ddocs=disabled
    CC: gcc
    CXX: g++



# Packaging
.xrdesktop.packaging.conditions:
  rules:
    # Only the default branch of the "upstream" repo.
    - if: $CI_PROJECT_PATH =~ /(xrdesktop\/.*|haagch\/.*)/ && $CI_COMMIT_REF_NAME =~ /(main|legacy|release-.*)/
      when: on_success
    # Otherwise, don't build packages.
    - when: never


# Packaging
.xrdesktop.base-job.debuild:
  extends:
    - .xrdesktop.packaging.conditions
  stage: package
  before_script:
    # Configure git - needed despite not actually committing here.
    - git config --global user.email "christoph.haag@collabora.com"
    - git config --global user.name "xrdesktop CI"
  script:
    # Prep the source tree
    - git clean -dfx
    - git remote -v
    - git fetch --unshallow
    - git fetch origin
    - git fetch origin --tags
    - rm -rf debian
    - cp -ra ${PACKAGE_DIR} debian
    - DEBFULLNAME="xrdesktop CI <christoph.haag@collabora.com>" DEBEMAIL="christoph.haag@collabora.com" debian/extra/prepare-commit-package.sh ${CI_COMMIT_SHA} 1~${BACKPORT_SUFFIX}~ci$(date --utc "+%Y%m%d")
    # Build the package
    - debuild -uc -us -d -sa
    # Use dput-ng to move the package-related files into some artifacts.
    - export INCOMING=$(pwd)/incoming
    - mkdir -p $INCOMING
    - mkdir -p ~/.dput.d/profiles
    - cat .gitlab-ci/localhost.json | envsubst  > ~/.dput.d/profiles/localhost.json
    - dpkg-parsechangelog --show-field version > incoming/${DISTRO}.distro
    - dput --force --debug localhost ../kwin-effect-xrdesktop_$(dpkg-parsechangelog --show-field version)_amd64.changes
  artifacts:
    paths:
      - "incoming/"
    expire_in: 2 days


debian:bookworm:package:
  extends:
    - .xrdesktop.variables.debian:bookworm
    - .fdo.distribution-image@debian # from ci-templates
    - .xrdesktop.base-job.debuild
  variables:
    BACKPORT_SUFFIX: bpo8
    PACKAGE_DIR:  .gitlab-ci/debian_sid
    DISTRO: bookworm


debian:bullseye:package:
  extends:
    - .xrdesktop.variables.debian:bullseye
    - .fdo.distribution-image@debian # from ci-templates
    - .xrdesktop.base-job.debuild
  variables:
    BACKPORT_SUFFIX: bpo9
    PACKAGE_DIR:  .gitlab-ci/debian_sid
    DISTRO: bullseye


debian:sid:package:
  extends:
    - .xrdesktop.variables.debian:sid
    - .fdo.distribution-image@debian # from ci-templates
    - .xrdesktop.base-job.debuild
  variables:
    BACKPORT_SUFFIX: bpo10
    PACKAGE_DIR:  .gitlab-ci/debian_sid
    DISTRO: sid


ubuntu:focal:package:
  extends:
    - .xrdesktop.variables.ubuntu:focal
    - .fdo.distribution-image@ubuntu # from ci-templates
    - .xrdesktop.base-job.debuild
  variables:
    BACKPORT_SUFFIX: ubuntu20.04
    PACKAGE_DIR:  .gitlab-ci/debian_sid
    DISTRO: focal


ubuntu:jammy:package:
  extends:
    - .xrdesktop.variables.ubuntu:jammy
    - .fdo.distribution-image@ubuntu # from ci-templates
    - .xrdesktop.base-job.debuild
  variables:
    BACKPORT_SUFFIX: ubuntu20.04
    PACKAGE_DIR:  .gitlab-ci/debian_sid
    DISTRO: jammy


reprepro:package:
  stage: reprepro
  extends:
    - .xrdesktop.variables.debian:sid
    - .xrdesktop.packaging.conditions
    - .fdo.distribution-image@debian # from ci-templates
  dependencies:
    - debian:bookworm:package
    - debian:bullseye:package
    - debian:sid:package
    - ubuntu:focal:package
    - ubuntu:jammy:package
  before_script:
    # Convince gnupg to work properly in CI
    - mkdir -p ~/.gnupg && chmod 700 ~/.gnupg
    - touch ~/.gnupg/gpg.conf
    - echo 'use-agent' > ~/.gnupg/gpg.conf
    - echo 'pinentry-mode loopback' >> ~/.gnupg/gpg.conf
    - touch ~/.gnupg/gpg-agent.conf
    - echo 'allow-loopback-pinentry' > ~/.gnupg/gpg-agent.conf
    - echo RELOADAGENT | gpg-connect-agent
    - gpg --batch --no-tty --yes --pinentry-mode loopback --passphrase ${XRDESKTOP_GPG_PASSPHRASE} --import ${XRDESKTOP_GPG_SECRET_KEY}

  script:
    # Use reprepro to create an apt repository in our artifacts
    - mkdir -p repo/conf
    # For each distro, sign the changes file and add it to the repo.
    - |
      for fn in incoming/*.distro; do
        # parse the distro name out
        export DISTRO=$(echo $fn | sed -e 's:incoming/::' -e 's:[.]distro::')
        echo "Processing $DISTRO"
        # add distro to repository config - blank line is mandatory!
        cat .gitlab-ci/distributions | envsubst >> repo/conf/distributions
        echo >> repo/conf/distributions

        echo "Signing package for $DISTRO"
        debsign -k ${XRDESKTOP_GPG_FINGERPRINT} -p "gpg --batch --no-tty --yes --pinentry-mode loopback --passphrase ${XRDESKTOP_GPG_PASSPHRASE}" incoming/kwin-effect-xrdesktop_$(cat $fn)_amd64.changes

        echo "Adding package for $DISTRO to the repository"
        reprepro -V --ignore=wrongdistribution -b repo include ${DISTRO} incoming/kwin-effect-xrdesktop_$(cat $fn)_amd64.changes
      done
  artifacts:
    paths:
      - "repo/"
    expire_in: 2 days

packaging:
  extends:
    - .xrdesktop.packaging.conditions
  stage: trigger_packaging
  trigger:
    project: xrdesktop/packaging
